<?php

use Illuminate\Database\Seeder;

class InterestTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interests_languages')->insert([
                    'lang' => "en",
                    'name' => "Acting"
                ],[
                    'lang' => "nl",
                    'name' => "Acteren"
                ],[
                    'lang' => "en",
                    'name' => "Animation"
                ],[
                    'lang' => "nl",
                    'name' => "Animatis"
        ]);

        DB::table('interests')->insert([

            'name' => "Acting"
        ],[

            'name' => "Animation"
        ],[

            'name' => "Baking"
        ],[

            'name' => "Blogging"
        ],[

            'name' => "Cabaret"
        ],[

            'name' => "Calligraphy"
        ],[

            'name' => "Collecting"
        ]);

    }
}
