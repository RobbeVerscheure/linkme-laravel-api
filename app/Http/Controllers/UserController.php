<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function getAllUsers(){
        $users= User::all();
        return $users;
    }

    
    public function getInterestsFromUser(Request $request){
        $id = $request -> id;

        $user = User::find($id);

        $interests = $user->interests;
        return $interests;
    }

}
