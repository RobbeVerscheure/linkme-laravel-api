<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;
use Psy\Util\Json;

class RegisterController extends Controller
{

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed'
        ]);


        $validatedData['password'] = bcrypt($request->password);

        $user = User::create($validatedData);

        $profile = Profile::create();

        $profile -> id = $user->id;

        $profile->save();

        $accessToken = $user->createToken('authToken')->accessToken;


        return response()->json(['id' => $profile -> id]);

    }


}
