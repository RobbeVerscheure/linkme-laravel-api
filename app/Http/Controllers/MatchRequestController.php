<?php

namespace App\Http\Controllers;

use App\Match;
use App\MatchRequest;
use App\Profile;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class MatchRequestController extends Controller
{
    //

    function requestMatch(Request $request){
        $id1 = $request -> profile1;
        $id2 = $request -> profile2;

        $request1 = MatchRequest::where([['profile_1',$id1],['profile_2',$id2]])-> first();

        $request2 = MatchRequest::where([['profile_1',$id2],['profile_2',$id1]])-> first();

      if (!$request1 && !$request2){
          // Other user has not yet requested a link with this user
            $newRequest = MatchRequest::create();

            $newRequest -> profile_1 = $id1;
            $newRequest -> profile_2 = $id2;

            $newRequest -> save();

            return response()->json(["match" => false]);
        }
        else {
            // Other user has already requested a link with this user
            $newMatch = Match::create();

            $newMatch -> profile_1 = $id1;
            $newMatch -> profile_2 = $id2;

            $newMatch -> save();

            $nameMatched = User::where('id',$id2)->select('name')->first();

            return response()->json(["match" => true,"name"=>$nameMatched]);
        }

    }
}
