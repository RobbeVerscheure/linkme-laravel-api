<?php

namespace App\Http\Controllers;

use App\Match;
use Illuminate\Http\Request;

class MatchController extends Controller
{

    function getMatchesFromUserId(Request $request){
       $matches[] =  Match::where([['profile_1',$request->id]])->get();

        $matches[]  = Match::where([['profile_2',$request->id]])->get();

        return $matches;
    }
}
