<?php

namespace App\Http\Controllers;

use App\Interest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;

class InterestController extends Controller
{
    public function getAllInterests(Request $request){
        $allMessages = Interest::all();

        return $allMessages;
    }

    // Assign all interests to user
    public function addInterestToUser(Request $request){
        $interests =
            $request -> interests;

        $userId = $request -> id;

        foreach ($interests as $interest) {
            $interestFound = Interest::where('name',$interest)->first();
            DB::table("interest_user")->insert(['user_id'=> $userId[0],'interest_id' => $interestFound->id]);
        }
    }


}
