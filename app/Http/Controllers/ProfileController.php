<?php

namespace App\Http\Controllers;


use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{

    function changeProfilePicture(Request $request)
    {
        $id = $request->id;
        $profile = Profile::find($id);

        return $profile;
    }


    function fillProfile(Request $request)
    {

        $validatedData = $request->validate([
            'birthDate' => 'required',
            'gender' => 'required',
        ]);

        $id = $request->id;
        $birthDate = $validatedData["birthDate"];
        $gender = $validatedData["gender"];

        $profile = Profile::find($id);

        $profile->gender = $gender;
        $profile->birth_date = $birthDate;

        $profile->save();

        return response()->json(["Message" => "Profile has been filled!"]);
    }

    function updatePreferences(Request $request)
    {
        $id = $request->id;
        $prefGender = $request->gender;
        $prefDistance = $request->distance;

        $profile = Profile::find($id);
        $profile->preferred_gender = $prefGender;
        $profile->radius = $prefDistance;

        $profile->save();
        return response()->json(["Message" => "Profile has been updated!"]);
    }

    // Get new profile that has not already been suggested to this user
    function suggestProfile(Request $request)
    {
        $id = $request->id;

        $userProfile = Profile::find($id);

        $suggestedIds = DB::table('has_been_suggested_to')->select('second_user')->where('first_user', $id);

        $sugProfile = DB::table('user_profiles')->select('*')->where('gender', $userProfile->preferred_gender)->whereNotIn('id', $suggestedIds)->first();


        $user = User::where('id', $sugProfile->id)->select('name')->first();

        $this->addToSuggested($id, $sugProfile->id);


        return response()->json(["profile" => $sugProfile, "user" => $user]);
    }

    private function addToSuggested(int $first, int $second)
    {
        DB::table('has_been_suggested_to')->insert(['first_user' => $first, 'second_user' => $second]);
    }

    function getProfile(Request $request)
    {

        $id = $request->id;

        $profile = Profile::find($id);
        $user = User::find($id);

        return response()->json(["profile" => $profile, "user" => $user]);
    }
}
