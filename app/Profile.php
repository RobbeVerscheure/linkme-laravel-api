<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $table = "user_profiles";

    protected $fillable = [
        'id',
        'gender',
        'birth_date',
        'description',
        'country',
        'preferred_gender',
        'radius',
        'profile_picture'
    ];
}
