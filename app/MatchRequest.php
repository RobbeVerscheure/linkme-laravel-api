<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchRequest extends Model
{
    //
    protected $table = "match_request";

    protected $fillable = [
        'profile_1',
        'profile_2'
    ];
}
