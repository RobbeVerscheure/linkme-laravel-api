<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('/user')->group(function () {
    Route::post('/login', 'LoginController@login');
    Route::post('/register', 'RegisterController@register');

});

Route::prefix('/interest')->group(function () {
    Route::get('/all', 'InterestController@getAllInterests');
    Route::get('/fromUser/{id}', 'UserController@getInterestsFromUser');
    Route::post('/add', 'InterestController@addInterestToUser');
});

Route::prefix('/user')->group(function () {
    Route::get('/all', 'UserController@getAllUsers');
});

Route::prefix('/match')->group(function () {
    Route::post('/request', "MatchRequestController@requestMatch");
    Route::post('/all', "MatchController@getMatchesFromUserId");
});


Route::prefix('/profile')->group(function(){
    Route::post('/get',"ProfileController@getProfile");
    Route::post('/fill',"ProfileController@fillProfile");
    Route::post('/preference/add',"ProfileController@updatePreferences");
    Route::post('/suggest',"ProfileController@suggestProfile");
});



