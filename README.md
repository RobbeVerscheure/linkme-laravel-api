# LinkMe-Laravel-API

LinkMe is a new dating app that connects people based on their interests. 
People can logon to the app and the app will suggest some profiles. 
It is then the choice of the user to start a conversation with the person or not.

## Technical description
This project was made in Laravel. The base function of this project is to serve as an API.

### Actions per controller
In this little chapter, each controller is written down with it's own responsibilities.

#### Authentication controllers
It's possible to login or register by using this API.
Every action has it's own controller. These are now set apart, but both functions could be put into one.

- Login controller
- Register controller

#### InterestController
The interest controller is in charge of saving and retrieving the interests a person has set to their profile.
- Get all interests from profile
- Add all interests to profile

#### MatchController
The match controller makes it possible to get all the links (matches) that have been set between users
- Get all matches from profile

#### MatchRequestController
The match requests need to be saved to make sure a match is made when both profiles have given a green light. These requests are managed by this controller. The controller checks if a an opposite request has already been made. If it is, a match will be made. If not, the request will be saved for later.

- Request a match





